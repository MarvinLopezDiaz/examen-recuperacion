let btnGenerar = document.getElementById('generar');
let btnCalcular = document.getElementById('calcular');
let btnRegistrar = document.getElementById('registrar');
let btnBorrarR = document.getElementById('borrarR');
var numero = 1;
var ImcTotal = 0;
var ImcPro = 0;

function random() {
    return Math.floor((Math.random() * (99 - 18 + 1)) + 18);
}

function random2() {
    return (Math.random() * ((2.5 - 1.5)) + 1.5).toFixed(1);
}

function random3() {
    return Math.floor((Math.random() * (130 - 20 + 1)) + 20);
}

function GenrerRandom(){
    var numRan = random();
    var numRan2 = random2();
    var numRan3 = random3();

    document.getElementById('edad').value = numRan;
    document.getElementById('altura').value = numRan2;
    document.getElementById('peso').value = numRan3;
}

function CalcularMC(){
    let al = document.getElementById('altura').value;
    let pe = document.getElementById('peso').value;

    var maCorporal = pe / (al*al);

    if(maCorporal< 18.5){
        peso = "Bajo peso"
        document.getElementById('nivel').value = peso;
    } else if(maCorporal > 18.5 && maCorporal < 24.9){
        peso = "Peso saludable"
        document.getElementById('nivel').value = peso;
    } else if(maCorporal > 25.0 && maCorporal < 29.9){
        peso = "Sobrepeso"
        document.getElementById('nivel').value = peso;
    }
    else if(maCorporal > 30){
        peso = "Obesidad"
        document.getElementById('nivel').value = peso;
    }
    document.getElementById('imc').value = maCorporal.toFixed(1);
    ImcTotal = ImcTotal + maCorporal;
    console.log(ImcTotal)
}

function registrarTabla(){
    let ed = document.getElementById('edad').value;
    let al = document.getElementById('altura').value;
    let pe = document.getElementById('peso').value;
    let Imc = document.getElementById('imc').value;
    let ni = document.getElementById('nivel').value;

    let NumTa = document.getElementById('numTabla');
    let EdTa = document.getElementById('edadTabla');
    let AlTa = document.getElementById('alturaTabla');
    let PeTa = document.getElementById('pesoTabla');
    let ImcTa = document.getElementById('imcTabla');
    let NiTa = document.getElementById('nivelTabla');
    let Promedio = document.getElementById('promedio');

    Promedio.innerHTML = 'IMC Promedio: ';

    NumTa.innerHTML += numero + '<br>';
    EdTa.innerHTML += ed + '<br>';
    AlTa.innerHTML += al + '<br>';
    PeTa.innerHTML += pe + '<br>';
    ImcTa.innerHTML += Imc + '<br>';
    NiTa.innerHTML += ni + '<br>';
    numero++;
    ImcPro = (ImcTotal/numero).toFixed(2);
    Promedio.innerHTML += ImcPro;
}

function BorrarTodo(){
    let ed = document.getElementById('edad').value = '';
    let al = document.getElementById('altura').value = '';
    let pe = document.getElementById('peso').value = '';
    let Imc = document.getElementById('imc').value = '';
    let ni = document.getElementById('nivel').value = '';

    let NumTa = document.getElementById('numTabla').innerHTML = '';
    let EdTa = document.getElementById('edadTabla').innerHTML = '';
    let AlTa = document.getElementById('alturaTabla').innerHTML = '';
    let PeTa = document.getElementById('pesoTabla').innerHTML = '';
    let ImcTa = document.getElementById('imcTabla').innerHTML = '';
    let NiTa = document.getElementById('nivelTabla').innerHTML = '';
    let Promedio = document.getElementById('promedio').innerHTML = 'IMC Promedio: ';
    numero = 1;
    ImcTotal = 0;    
    ImcPro = 0;
}
